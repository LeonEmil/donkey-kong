const musicScreenTitle = new Audio('https://res.cloudinary.com/leonemil/video/upload/v1578182650/Donkey%20kong/title_fnurdd.mp3')
const musicStage = new Audio('https://res.cloudinary.com/leonemil/video/upload/v1578182747/Donkey%20kong/stage_cf5hcg.mp3')
const walking = new Audio('https://res.cloudinary.com/leonemil/video/upload/v1578182638/Donkey%20kong/walking_zxwng0.wav')

const screen = document.getElementById('screen')

playMusic.addEventListener('click', () => {
    musicScreenTitle.play()
    screen.style.display = "none"
    level.style.display = "none"
    titleScreen.style.display = "flex"
    setTimeout(() => {
        titleScreen.style.display = "none"
        level.style.display = "initial"
        musicStage.play()
    }, 15000);
})


const mario = document.getElementById("mario")

let x = 0
let y = 0

const move = (direction) => {
    switch (direction) {

        case "left":
            x--
            break;
        case "right":
            x++
            break;
        case "up":
            y++
            mario.style.transform = `translateY(${y*7}px)`
            break;
    
        default:
            break;
    }

     mario.style.transform = `translate(${x*7}px)`
}

if(mario){
    addEventListener("keydown", (e) => {
            console.log(e.key)
        switch (e.key) {
            
            case "ArrowLeft":
                move("left")
                walking.play()
                mario.classList.add('mario--walking')
                mario.classList.add('mario--back')
                break;
            case "ArrowRight":
                move("right")
                walking.play()
                mario.classList.add('mario--walking')
                mario.classList.remove('mario--back')
                break;

            case "ArrowDown":
                mario.classList.remove('mario--walking')
                break;
                default:
                break;
            case "ArrowUp":
                move("up")
                walking.play()
                mario.classList.remove('mario--walking')
                break;
        }
    })
}
